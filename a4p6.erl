%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. Oct 2017 5:03 PM
%%%-------------------------------------------------------------------
-module(a4p6).
-author("scottwillmarth").

%% API
-export([run/2]).

run(List, keyword) ->
  E = [X|| {keyword, X} <- List],
  io:format(E).
