%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. Oct 2017 4:19 PM
%%%-------------------------------------------------------------------
-module(a4p4).
-author("scottwillmarth").

%% API
-export([countTo/1]).
-export([begCounting/3]).

countTo(Number) ->
  begCounting(Number, 0, []).

begCounting(Number, Counter, []) -> begCounting(Number, Counter, [Counter]);
begCounting(Number, Counter, AList) ->
  if
    Number == Counter -> AList;
    Number > Counter -> TempList = AList ++ [Counter+1], begCounting(Number, Counter+1, TempList)
  end.

