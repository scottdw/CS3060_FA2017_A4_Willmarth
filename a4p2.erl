%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. Oct 2017 3:50 PM
%%%-------------------------------------------------------------------
-module(a4p2).
-author("scottwillmarth").

%% API
-export([count/2]).
-export([begCount/1]).

count([], Count) -> Count;
count([_ | T], Count) -> count(T, Count + 1).

begCount(Word) -> count(Word, 0).
