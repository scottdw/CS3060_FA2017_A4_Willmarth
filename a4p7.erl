%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Oct 2017 12:45 PM
%%%-------------------------------------------------------------------
-module(a4p7).
-author("scottwillmarth").

%% API
-export([createMatrix/1]).
-export([matrixMult/1]).
-export([transpose/1]).
-export([red/2]).
-export([dot/2]).
-export([multiply/2]).
-export([maltiply/2]).
-export([mRC/2]).

createMatrix(N) ->
  A = [[random:uniform(N) || _ <- lists:seq(1, N)] || _ <- lists:seq(1, N)],
  A.

matrixMult(X) ->
  A = createMatrix(X),
  B = createMatrix(X),
  multiply(A,B).

transpose([[]|_]) -> [];
transpose(B) -> [lists:map(fun hd/1, B) | transpose(lists:map(fun tl/1, B))].

red(Pair, Sum) ->
  X = element(1, Pair),   %gets X
  Y = element(2, Pair),   %gets Y
  X * Y + Sum.

dot(A, B) -> lists:foldl(fun red/2, 0, lists:zip(A, B)).


multiply(A, B) -> maltiply(A, transpose(B)).

maltiply([Head | Rest], B) ->
  % multiply each row by Y
  Element = mRC(Head, B),
  [Element | maltiply(Rest, B)];

maltiply([], B) -> [].

mRC(Row, [Col_Head | Col_Rest]) ->
  Scalar = dot(Row, Col_Head),
  [Scalar | mRC(Row, Col_Rest)];

mRC(Row, []) -> [].