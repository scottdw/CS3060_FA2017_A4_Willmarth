%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. Oct 2017 2:32 PM
%%%-------------------------------------------------------------------
-module(a4p9).
-author("scottwillmarth").

%% API
-export([chat/0]).
-export([talk/1]).
-export([discuss/0]).
-export([start/1]).

talk([0, Chat_PID, Discuss_PID]) ->
  Chat_PID ! {finished, Discuss_PID},
  io:format("talk has finished~n", []);

talk([N, Chat_PID, Discuss_PID]) -> % Takes Chat and Discuss ID
  Chat_PID ! [{talk, self()},{discuss, Discuss_PID}], %Sends Talk_PID and Discuss_PID to chat
  receive
    [{chat, Chat_PID}, {discuss, Discuss_PID}] ->
      io:format("talk received chat~n", [])
  end,
  talk([N - 1, Chat_PID, Discuss_PID]).

chat() ->
  receive
    {finished, Discuss_PID} ->
      Discuss_PID ! finished,
      io:format("chat has finished~n", []);

    [{talk, Talk_PID}, {discuss, Discuss_PID}] -> %receives talk and discuss
      io:format("chat received from talk going to discuss~n", []),
      Discuss_PID ! [{chat, self()}, {talk, Talk_PID}], %Sends Chat_PID and Talk_PID to discuss
      chat();

    [{discuss, Discuss_PID}, {talk, Talk_PID}] -> %receives discuss and talk
      io:format("chat received from discuss going to talk", []),
      Talk_PID ! [{chat, self()}, {discuss, Discuss_PID}],
      chat()
  end.

discuss() ->
  receive
    finished ->
      io:format("discuss has finished~n", []);

    [{chat, Chat_PID}, {talk, Talk_PID}] -> % receives chat and talk
      io:format("discuss received chat~n", []),
      Chat_PID ! [{discuss, self()}, {talk, Talk_PID}], %%Sends Discuss_PID and Talk_PID  to chat
      discuss()
  end.

start(N) ->
  Chat_PID = spawn(attempt, chat, []),
  Discuss_PID = spawn(attempt, discuss, []),
  spawn(attempt, talk, [N, Chat_PID, Discuss_PID]).