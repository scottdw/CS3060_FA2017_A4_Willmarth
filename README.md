In order to run problem 1, call helloWorld(),  
Hello, world!

In order to run problem 2, call begCount(ValName) or begCount("string"), 
"string" -> 6

In order to run problem 3, call wordCount(AList)
[a,b,c] -> 3
["One word", "two word", "three word", "four word"] -> 4

In order to run problem 4, call countTo(ValName or number), 
4 -> [0,1,2,3,4]

In order to run problem 5, call isError(error/success, message), 
(error, hello) -> error: hello
(succes, hi) -> success

In order to run problem 6, you must (A). Have a list/tuple (B). change the atom within the code to the atom you will be searching for because the list_to_atom() function does not work where it needs to work (C) call run(ListName, atom)
AList = [{erlang,  "a functional language" }, {ruby,  "an OO language" }].
run(AList, erlang). -> "a functional language"

In order to run problem 7, call matrixMul(100)
3 -> [[3,3,2],[1,3,1],[2,3,3]] X [[2,3,3],[1,2,1],[2,3,2]] = [[13,21,16],[7,12,8],[13,21,15]]

In order to run problem 8, call fib(Num)
0 -> 0
1 -> 1
1 -> 1
5 -> 5
10 -> 55

In order to run problem 9, call start(N)
2-> 
chat received from talk going to discuss
discuss received chat
chat received from discuss going to talk
talk received chat
chat received from talk going to discuss
discuss received chat
chat received from discuss going to talk
talk received chat
talk has finished
chat has finished
discuss has finished

In order to run problem 10, call build(N)
3 -> 
Ring was built
Received Message 3 
Received Message 2 
Received Message 1 
It finished
