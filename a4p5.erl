%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. Oct 2017 5:03 PM
%%%-------------------------------------------------------------------
-module(a4p5).
-author("scottwillmarth").

%% API
-export([isError/2]).

isError(success, Message) ->
  io:format("success \n");
isError(error, Message) ->
  io:format("error: ~s \n", [Message]).