%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 06. Oct 2017 3:30 PM
%%%-------------------------------------------------------------------
-module(a4p1).
-author("scottwillmarth").

%% API
-export([helloWorld/0]).

helloWorld() -> io:format("Hello, world! \n").