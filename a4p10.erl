%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 21. Oct 2017 4:45 PM
%%%-------------------------------------------------------------------
-module(a4p10).
-author("scottwillmarth").

%% API

-export([build/1, ring_node/2, loop/1, send_messages/1]).

build(N) ->
  Self = self(),
  register(ring, Self),
  Root = spawn(?MODULE, ring_node, [null, Self]),
  Root ! {create, N},
  loop(N).

loop(M) ->
  receive
    ok ->
      io:format("Ring was built~n"),
      send_messages(M),
      loop(M);

    {message, finished} -> io:format("Shutting down ~n");

    {message, _} ->
      loop(M)
  end.

send_messages(0) ->
  ring ! {message, finished};

send_messages(M) ->
  io:format("Received Message ~p ~n", [M]),
  ring ! {message, M},
  send_messages(M-1).

ring_node(ChildPid, Manager) ->
  receive
    {create, 0} ->
      Manager ! ok,
      ring_node(ChildPid, Manager);

    {create, N} ->
      Child = spawn(?MODULE, ring_node, [null, Manager]),
      Child ! {create, N-1},
      ring_node(Child, Manager)
  end.