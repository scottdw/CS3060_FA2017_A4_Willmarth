%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. Oct 2017 4:19 PM
%%%-------------------------------------------------------------------
-module(a4p3).
-author("scottwillmarth").

%% API
-export([wordNum/1]).
-export([wordNum/2]).

wordNum(List) -> wordNum(List, 0).
wordNum([], Acc) -> Acc;
wordNum([_|T], Acc) -> wordNum(T, Acc + 1).

