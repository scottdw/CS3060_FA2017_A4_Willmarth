%%%-------------------------------------------------------------------
%%% @author scottwillmarth
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 18. Oct 2017 5:52 PM
%%%-------------------------------------------------------------------
-module(a4p8).
-author("scottwillmarth").

%% API
-export([fib/1]).

fib(0) -> 0;
fib(1) -> 1;
fib(2) -> 1;
fib(Num) -> fib(Num-1) + fib(Num-2).

